﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class my_profile
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string email_address { get; set; }
        public string mobile_no { get; set; }
        public string address { get; set; }
        public string dob { get; set; }
        public string doj { get; set; }
        public string qualification { get; set; }
        public string fullname { get; set; }

        public string zipcode { get; set; }
        public string street { get; set; }

        public string doj_date { get; set; }
        public string doj_month { get; set; }
        public int doj_year { get; set; }

        public string dob_date { get; set; }
        public string dob_month { get; set; }
        public int dob_year { get; set; }

        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }

    }
}
