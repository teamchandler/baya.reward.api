﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class ImportParticipant
    {
        public long participant_id { get; set; }
        public string participant_code { get; set; }
        public int participant_type { get; set; }
        public string parent_category { get; set; }
        public string participant_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string contact_no1 { get; set; }
        public string contact_no2 { get; set; }
        public string email1 { get; set; }
        public string email2 { get; set; }
        public int is_active { get; set; }

        public string store { get; set; }
        public DateTime registration_date { get; set; }

        public string primary_company { get; set; }
        public string primary_distributor { get; set; }
        public string primary_retailer { get; set; }

        public int type_id { get; set; }
        public string Owner_Name { get; set; }
        public string RM_Name { get; set; }
        public string region { get; set; }
        public string user_type { get; set; }
        public string comp_name { get; set; }

        public string parent_participant { get; set; }
    }

    public class SI_Data
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public string location { get; set; }
        public string user_status { get; set; }
        public string registration_date { get; set; }
        public string token_id { get; set; }
        public string message { get; set; }
        public string total_point { get; set; }
        public string company { get; set; }
        public string CompanyId { get; set; }

        public string email_address { get; set; }
        public string company_name { get; set; }
        public string designation { get; set; }
        public string retailer_code { get; set; }
        public string distributor_code { get; set; }
        public string zone { get; set; }
        public string district { get; set; }
        public string participant_id { get; set; }

    }



    public class Sales_Dump
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string Company_Code { get; set; }
        public string Document_Number { get; set; }
        public DateTime Document_Date { get; set; }
        public string Customer_Code { get; set; }
        public string Customer_Name { get; set; }
        public string Agent_Code { get; set; }
        public string Agent_Name { get; set; }
        public string Customer_Group { get; set; }
        public string Customer_Subgroup { get; set; }
        public string Product { get; set; }
        public string Product_Description { get; set; }
        public string Product_Group { get; set; }
        public string Product_Subgroup { get; set; }
        public string Warehouse { get; set; }
        public string Warehouse_Name { get; set; }
        public string Rack_No { get; set; }
        public string Segment1 { get; set; }
        public string Segment2 { get; set; }
        public string Segment3 { get; set; }
        public string Segment4 { get; set; }
        public string Segment5 { get; set; }
        public string Segment6 { get; set; }
        public string Segment7 { get; set; }
        public string Sales_Tax { get; set; }
        public int Quantity { get; set; }
        public double Rate { get; set; }
        public double Amount { get; set; }
        public double Product_Discount { get; set; }
        public double Additional_Disocunt { get; set; }
        public double Taxable_Amount { get; set; }
        public double VAT_TAX { get; set; }
        public double Ed_Cess_At_2Per { get; set; }
        public double High_Edu_Cess_1Per { get; set; }
        public double Freight_Charges { get; set; }
        public double Round_On_Off { get; set; }
        public double Sales_Total_Amount { get; set; }

        public DateTime created_date { get; set; }
        public int active { get; set; }


    }


    public class distributor_sales_data
    {
        public string Company_Code { get; set; }
        public string Document_Number { get; set; }
        public string Document_Date { get; set; }
        public string Customer_Code { get; set; }
        public string Customer_Name { get; set; }
        public string Agent_Code { get; set; }
        public string Agent_Name { get; set; }
        public string Customer_Group { get; set; }
        public string Customer_Subgroup { get; set; }
        public string Product { get; set; }
        public string Product_Description { get; set; }
        public string Product_Group { get; set; }
        public string Product_Subgroup { get; set; }
        public string Warehouse { get; set; }
        public string Warehouse_Name { get; set; }
        public string Rack_No { get; set; }
        public string Segment1 { get; set; }
        public string Segment2 { get; set; }
        public string Segment3 { get; set; }
        public string Segment4 { get; set; }
        public string Segment5 { get; set; }
        public string Segment6 { get; set; }
        public string Segment7 { get; set; }
        public string Sales_Tax { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string Product_Discount { get; set; }
        public string Additional_Disocunt { get; set; }
        public string Taxable_Amount { get; set; }
        public string VAT_TAX { get; set; }
        public string Ed_Cess_At_2Per { get; set; }
        public string High_Edu_Cess_1Per { get; set; }
        public string Freight_Charges { get; set; }
        public string Round_On_Off { get; set; }
        public string Sales_Total_Amount { get; set; }


    }


    public class sku_master_data
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string brand { get; set; }
        public string sku { get; set; }
        public string description { get; set; }
        public int active { get; set; }
        public DateTime created_date { get; set; }

    }

    public class retailer_master
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string contact_number1 { get; set; }
        public string contact_number2 { get; set; }
        public string email1 { get; set; }
        public string email2 { get; set; }
        public string code { get; set; }
        public int active { get; set; }
        public DateTime created_date { get; set; }
    }


    public class sku_wise_tran
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string sku { get; set; }
        public string from_participant_code { get; set; }
        public string from_participant { get; set; }
        public string to_participant_code { get; set; }
        public string to_participant { get; set; }
        public int flow_type { get; set; }// 1 : From Company To Distributor , 2 : From Distributor To Retailer
        public string tran_type { get; set; }// c : Credit , d : Debit
        public int quantity { get; set; }
        public int credit_quantity { get; set; }
        public int debit_quantity { get; set; }
        public int active { get; set; }
        public DateTime tran_date { get; set; }
        public DateTime created_date { get; set; }

    }

    public class hierarchy_conf
    {
        public int call_for { get; set; }
        public int type_id { get; set; }
        public int parent_id { get; set; }
        public string type_desc { get; set; }
        public int depth { get; set; }
        public int status { get; set; }
        public string created_by { get; set; }
        public DateTime created_ts { get; set; }



    }

    public class hierarchy
    {

        public string _id { get; set; }
        public string parent_id { get; set; }
        public string type_id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public string extra { get; set; }
        public string created_by { get; set; }
        public string created_ts { get; set; }
        public string modified_by { get; set; }
        public string modified_ts { get; set; }
        public string deleted { get; set; }

    }




}
