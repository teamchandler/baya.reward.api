﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class Contact
    {

        public long contact_us_id { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string telephone { get; set; }
        public string comments { get; set; }
        public string company { get; set; }


    }
}
