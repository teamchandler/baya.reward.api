﻿using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.model.repository
{
    public class hierarchy_repository
    {

        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {

                return DateTime.Parse("1/1/1800");
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<OutCollection> ImportParticipants(ImportParticipant oip)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {
                guid = Guid.NewGuid().ToString();
                Pwd = GenerateSIUniquePassword();
                Pin = GenerateUniquePin();

                int i = da.ExecuteSP("import_participant_data", ref oc
                    , da.Parameter("_participant_id", convertToLong(oip.participant_id))
                    , da.Parameter("_participant_code", convertToString(oip.participant_code))
                    , da.Parameter("_participant_type", convertToInt(oip.participant_type))
                    , da.Parameter("_parent_category", convertToString(oip.parent_category))
                    , da.Parameter("_participant_name", convertToString(oip.participant_name))
                    , da.Parameter("_address", convertToString(oip.address))
                    , da.Parameter("_city", convertToString(oip.city))
                    , da.Parameter("_state", convertToString(oip.state))
                    , da.Parameter("_country", convertToString(oip.country))
                    , da.Parameter("_contact_no1", convertToString(oip.contact_no1))
                    , da.Parameter("_contact_no2", convertToString(oip.contact_no2))
                    , da.Parameter("_email1", convertToString(oip.email1))
                    , da.Parameter("_email2", convertToString(oip.email2))
                    , da.Parameter("_is_active", convertToInt(oip.is_active))
                    , da.Parameter("_store", convertToString(oip.store))
                    , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_primary_company", 0)
                    , da.Parameter("_primary_distributor", 0)
                    , da.Parameter("_primary_retailer", 0)
                    , da.Parameter("_parent_1_id", "")
                    , da.Parameter("_parent_1_type", 0)
                    , da.Parameter("_parent_2_id", "")
                    , da.Parameter("_parent_2_type", 0)
                    , da.Parameter("_parent_3_id", "")
                    , da.Parameter("_parent_3_type", 0)
                    , da.Parameter("_parent_4_id", "")
                    , da.Parameter("_parent_4_type", 0)
                    , da.Parameter("_type_id", convertToInt(oip.type_id))
                    , da.Parameter("_hierarchy_id", 0)
                    , da.Parameter("_Owner_Name", convertToString(oip.Owner_Name))
                    , da.Parameter("_RM_Name", convertToString(oip.RM_Name))
                    , da.Parameter("_password", Pwd)
                    , da.Parameter("_guid", guid)
                    , da.Parameter("_region", convertToString(oip.region))
                    , da.Parameter("_USER_TYPE", convertToString(oip.user_type))
                    , da.Parameter("_comp_name", convertToString(oip.comp_name))
                    , da.Parameter("_parent_participant", convertToString(oip.parent_participant))
                    , da.Parameter("_first_name", convertToString(oip.first_name))
                    , da.Parameter("_last_name", convertToString(oip.last_name))
                    , da.Parameter("_outmsg", String.Empty, true));


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }



        public List<OutCollection> ImportSIData(SI_Data sd)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GenerateSIUniquePassword();
                Pin = GenerateUniquePin();
                int i = da.ExecuteSP("import_si_data", ref oc
                     , da.Parameter("_first_name", convertToString(sd.first_name))
                     , da.Parameter("_last_name", convertToString(sd.last_name))
                     , da.Parameter("_mobileno", convertToString(sd.mobile_no))
                     , da.Parameter("_email_id", convertToString(sd.email_id))
                     , da.Parameter("_email_address", convertToString(sd.email_address))
                     , da.Parameter("_company_name", convertToString(sd.company_name))
                     , da.Parameter("_designation", convertToString(sd.designation))
                     , da.Parameter("_Retailer_code", convertToString(sd.retailer_code))
                     , da.Parameter("_Distributor_code", convertToString(sd.distributor_code))
                     , da.Parameter("_region", convertToString(sd.zone))
                     , da.Parameter("_address", convertToString(sd.address))
                     , da.Parameter("_source", "excel_upload")
                     , da.Parameter("_password", Pwd)
                     , da.Parameter("_status", 9)
                     , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                     , da.Parameter("_company_id", convertToLong(sd.CompanyId))
                     , da.Parameter("_company", "")
                     , da.Parameter("_guid", guid)
                     , da.Parameter("_city", convertToString(sd.city))
                     , da.Parameter("_state", convertToString(sd.state))
                     , da.Parameter("_participant_id", convertToString(sd.participant_id))
                     , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;
        }

        private string GenerateSIUniquePassword()
        {
            try
            {
                int maxSize = 3;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private string GenerateUniquePin()
        {
            try
            {
                int maxSize = 5;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public List<ImportParticipant> PopParticipants(ImportParticipant oip)
        {

            DataAccess da = new DataAccess();

            List<ImportParticipant> lcomp = new List<ImportParticipant>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_participant_details"
                                , da.Parameter("_type", convertToInt(oip.participant_type))
                                , da.Parameter("_participant_code", oip.participant_code)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ImportParticipant comp = new ImportParticipant();
                        comp.participant_code = convertToString(dtResult.Rows[i]["participant_code"]);
                        comp.participant_name = convertToString(dtResult.Rows[i]["participant_name"]);
                        lcomp.Add(comp);
                    }
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        public List<COMPANY> PopCompany()
        {

            DataAccess da = new DataAccess();

            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                , da.Parameter("_comp_id", 0)
                                , da.Parameter("_entry_mode", "N")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["COMPANY_ID"].ToString());
                        comp.name = dtResult.Rows[i]["COMPANY_NAME"].ToString();
                        lcomp.Add(comp);
                    }
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        public string ImportSalesDump(distributor_sales_data odsd)
        {
            DataAccess da = new DataAccess();
            string oc = "";

            try
            {


                //****************************Checking Whether The Disti Is Present Or Not In Our db****************************************//
                DataTable dt = get_participant_data(2, convertToString(odsd.Customer_Code));
                bool process_cond = false;
                if (dt != null && dt.Rows.Count > 0)
                {
                    process_cond = true;
                }

                if (process_cond)//Disti Is Present
                {

                    dal.DataAccess dal = new DataAccess();

                    //****************************Check Already Process Doc By Doc No****************************************//
                    string doc_number = "";
                    bool already_process_cond = false;
                    doc_number = convertToString(odsd.Document_Number);
                    string qry = "{'Document_Number':'" + doc_number + "'}}";
                    BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                    QueryDocument qDoc = new QueryDocument(doc);
                    string[] in_fields = new string[] { "Document_Number" };
                    string[] ex_fields = new string[] { "_id" };
                    MongoCursor cursr = dal.execute_mongo_db("sales_dump", qDoc, in_fields, ex_fields);
                    foreach (var c in cursr)
                    {
                        string Doc_Number = check_field(c.ToBsonDocument(), "Document_Number") ? convertToString(c.ToBsonDocument()["Document_Number"]) : null;
                        if (Doc_Number != null && Doc_Number != "")
                        {
                            already_process_cond = true;
                            break;
                        }
                    }

                    if (already_process_cond)
                    {
                        oc = "This Document Is Already Processed";
                    }
                    else
                    {
                        //****************************Insert Sales Dump Entry Start****************************************//

                        Sales_Dump osd = new Sales_Dump();
                        osd._id = ObjectId.GenerateNewId().ToString();
                        osd.Company_Code = convertToString(odsd.Company_Code);
                        osd.Document_Number = convertToString(odsd.Document_Number);
                        osd.Document_Date = convertToDate(odsd.Document_Date);
                        osd.Customer_Code = convertToString(odsd.Customer_Code);
                        osd.Customer_Name = convertToString(odsd.Customer_Name);
                        osd.Agent_Code = convertToString(odsd.Agent_Code);
                        osd.Agent_Name = convertToString(odsd.Agent_Name);
                        osd.Customer_Group = convertToString(odsd.Customer_Group);
                        osd.Customer_Subgroup = convertToString(odsd.Customer_Subgroup);
                        osd.Product = convertToString(odsd.Product);
                        osd.Product_Description = convertToString(odsd.Product_Description);
                        osd.Product_Group = convertToString(odsd.Product_Group);
                        osd.Product_Subgroup = convertToString(odsd.Product_Subgroup);
                        osd.Warehouse = convertToString(odsd.Warehouse);
                        osd.Warehouse_Name = convertToString(odsd.Warehouse_Name);
                        osd.Rack_No = convertToString(odsd.Rack_No);
                        osd.Segment1 = convertToString(odsd.Segment1);
                        osd.Segment2 = convertToString(odsd.Segment2);
                        osd.Segment3 = convertToString(odsd.Segment3);
                        osd.Segment4 = convertToString(odsd.Segment4);
                        osd.Segment5 = convertToString(odsd.Segment5);
                        osd.Segment6 = convertToString(odsd.Segment6);
                        osd.Segment7 = convertToString(odsd.Segment7);
                        osd.Sales_Tax = convertToString(odsd.Sales_Tax);
                        osd.Quantity = convertToInt(odsd.Quantity);
                        osd.Rate = convertToDouble(odsd.Rate);
                        osd.Amount = convertToDouble(odsd.Amount);
                        osd.Product_Discount = convertToDouble(odsd.Product_Discount);
                        osd.Additional_Disocunt = convertToDouble(odsd.Additional_Disocunt);
                        osd.Taxable_Amount = convertToDouble(odsd.Taxable_Amount);
                        osd.VAT_TAX = convertToDouble(odsd.VAT_TAX);
                        osd.Ed_Cess_At_2Per = convertToDouble(odsd.Ed_Cess_At_2Per);
                        osd.High_Edu_Cess_1Per = convertToDouble(odsd.High_Edu_Cess_1Per);
                        osd.Freight_Charges = convertToDouble(odsd.Freight_Charges);
                        osd.Round_On_Off = convertToDouble(odsd.Round_On_Off);
                        osd.Sales_Total_Amount = convertToDouble(odsd.Sales_Total_Amount);
                        osd.created_date = System.DateTime.Now.ToLocalTime();
                        osd.active = 1;

                        BsonDocument bd_osd = osd.ToBsonDocument();
                        string sales_dump_result = dal.mongo_write("sales_dump", bd_osd);
                        if (sales_dump_result.Length > 0)
                        {
                            oc = "Record Inserted Successfully";
                        }
                        //****************************Insert Sales Dump Entry End****************************************//

                        //****************************Insert SKU In SKU Master Start****************************************//
                        string Product_SKU = "";
                        bool find_cond = false;
                        Product_SKU = convertToString(odsd.Product).ToUpper();
                        string q = "{'sku':'" + Product_SKU + "'}}";
                        BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                        QueryDocument queryDoc = new QueryDocument(document);
                        string[] include_fields = new string[] { "sku" };
                        string[] exclude_fields = new string[] { "_id" };
                        MongoCursor cursor = dal.execute_mongo_db("sku_master", queryDoc, include_fields, exclude_fields);
                        foreach (var c in cursor)
                        {
                            string SKU = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                            if (SKU != null && SKU != "")
                            {
                                find_cond = true;
                                break;
                            }
                        }

                        if (find_cond == false)
                        {
                            sku_master_data osmd = new sku_master_data();
                            osmd._id = ObjectId.GenerateNewId().ToString();
                            osmd.brand = "Blaupunkt";
                            osmd.sku = Product_SKU;
                            osmd.description = convertToString(odsd.Product_Description);
                            osmd.active = 1;
                            osmd.created_date = System.DateTime.Now.ToLocalTime();
                            BsonDocument bd_sku = osmd.ToBsonDocument();
                            string sku_result = dal.mongo_write("sku_master", bd_sku);
                        }

                        //****************************Insert SKU In SKU Master End****************************************//


                        //****************************Insert SKU Wise Transaction Start****************************************//

                        sku_wise_tran oswt = new sku_wise_tran();
                        oswt._id = ObjectId.GenerateNewId().ToString();
                        oswt.sku = Product_SKU;
                        oswt.from_participant_code = convertToString(odsd.Company_Code);
                        oswt.from_participant = "Blaupunkt";
                        oswt.to_participant_code = convertToString(odsd.Customer_Code);
                        oswt.to_participant = convertToString(odsd.Customer_Name);
                        oswt.quantity = convertToInt(odsd.Quantity);
                        oswt.credit_quantity = convertToInt(odsd.Quantity);
                        oswt.debit_quantity = 0;
                        oswt.flow_type = 1;
                        oswt.tran_type = "c";
                        oswt.active = 1;
                        oswt.tran_date = convertToDate(odsd.Document_Date);
                        oswt.created_date = System.DateTime.Now.ToLocalTime();
                        BsonDocument bd_sku_tran = oswt.ToBsonDocument();
                        string sku_tran_result = dal.mongo_write("sku_wise_tran", bd_sku_tran);

                        //****************************Insert SKU Wise Transaction End****************************************//
                    }
                }
                else
                {
                    oc = "Distributor Is Not Present In DB";
                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;
        }


        public DataTable get_participant_data(int participant_type, string participant_code)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_participant_details"
                                , da.Parameter("_type", convertToInt(participant_type))
                                , da.Parameter("_participant_code", participant_code)
                                );

                return dtResult;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }

        }

        public DataTable get_participant_hierarchy(int participant_type)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_user_data"
                                , da.Parameter("_type", convertToInt(participant_type)));

                return dtResult;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }

        }

        public DataTable get_hierarchy_config(hierarchy_conf ohc)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_hierarchy_config"
                                , da.Parameter("call_for", convertToInt(ohc.call_for))
                                , da.Parameter("curr_status", convertToInt(ohc.status))
                                , da.Parameter("type_id", convertToInt(ohc.type_id)));

                return dtResult;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }

        }
    }
}
