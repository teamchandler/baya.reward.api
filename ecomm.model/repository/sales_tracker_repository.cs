﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using System.Data;

namespace ecomm.model.repository
{
    public class sales_tracker_repository
    {
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        //private bool check_field(BsonDocument doc, string field_name)
        //{
        //    if (doc.Contains(field_name))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public List<sales_achievement_data> get_sales_achievement_data(string company, string user_id)
        {
            try
            {

                DataAccess da = new DataAccess();
                List<sales_achievement_data> s_list = new List<sales_achievement_data>();
                DataTable dtResult = new DataTable();
                dtResult = da.ExecuteDataTable("get_sales_achievement_data_by_user"
                                                , da.Parameter("_company", company)
                                                , da.Parameter("_user_id", user_id)
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        sales_achievement_data s = new sales_achievement_data();
                        s.cycle_name = dr["cycle_name"].ToString();
                        s.sales_cycle_id = convertToInt(dr["sales_cycle_id"].ToString());
                        s.sku = dr["sku"].ToString();
                        s.target_val = convertToInt(dr["target_val"].ToString());
                        s.achieve_val = convertToInt(dr["achieve_val"].ToString());
                        s.target_vol = convertToInt(dr["target_vol"].ToString());
                        s.achieve_vol = convertToInt(dr["achieve_vol"].ToString());
                        s.point = convertToInt(dr["point"].ToString());
                        s_list.Add(s);
                    }
                }
                return s_list;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }

        public work_profile get_work_profle(string user_id)
        {
            try
            {

                DataAccess da = new DataAccess();
                work_profile w = new work_profile();
                DataTable dtResult = new DataTable();
                dtResult = da.ExecuteDataTable("get_work_info"
                                , da.Parameter("_email_id", user_id)
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    w.fullname = dtResult.Rows[0]["fullname"].ToString();
                    w.branch_name = dtResult.Rows[0]["branch_name"].ToString();
                    w.branch_address = dtResult.Rows[0]["branch_address"].ToString();
                    w.manager_name = dtResult.Rows[0]["manager_name"].ToString();

                    w.emp_id = convertToString(dtResult.Rows[0]["emp_id"]);
                    w.Bu = convertToString(dtResult.Rows[0]["Bu"]);
                    w.Outlet_code = convertToString(dtResult.Rows[0]["Outlet_code"]);
                    w.Outlet_Name = convertToString(dtResult.Rows[0]["Outlet_Name"]);
                    w.Fos_Dse_Code = convertToString(dtResult.Rows[0]["Fos_Dse_Code"]);
                    w.Tm_Name = convertToString(dtResult.Rows[0]["Tm_Name"]);
                    w.Tm_code = convertToString(dtResult.Rows[0]["Tm_code"]);
                    w.city = convertToString(dtResult.Rows[0]["city"]);
                    w.District = convertToString(dtResult.Rows[0]["District"]);
                }
                return w;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }

        public my_profile get_my_profle(string user_id)
        {
            try
            {

                DataAccess da = new DataAccess();
                my_profile p = new my_profile();
                DataTable dtResult = new DataTable();
                dtResult = da.ExecuteDataTable("get_my_profile"
                                , da.Parameter("_email", user_id)
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    p.fullname = dtResult.Rows[0]["fullname"].ToString();
                    p.first_name = dtResult.Rows[0]["first_name"].ToString();
                    p.last_name = dtResult.Rows[0]["last_name"].ToString();
                    p.email_id = dtResult.Rows[0]["email_id"].ToString();
                    p.email_address = dtResult.Rows[0]["email_address"].ToString();
                    p.mobile_no = dtResult.Rows[0]["mobile_no"].ToString();
                    p.address = dtResult.Rows[0]["address"].ToString();
                    p.gender = dtResult.Rows[0]["gender"].ToString();
                    p.dob = dtResult.Rows[0]["dob"].ToString();
                    p.zipcode = convertToString(dtResult.Rows[0]["zipcode"]);

                    p.street = convertToString(dtResult.Rows[0]["street"]);

                    p.city = convertToString(dtResult.Rows[0]["city"]);
                    p.state = convertToString(dtResult.Rows[0]["state"]);
                    p.country = convertToString(dtResult.Rows[0]["country"]);

                    p.dob_date = dtResult.Rows[0]["dob_date"].ToString();
                    p.dob_month = dtResult.Rows[0]["dob_month"].ToString();
                    p.dob_year = convertToInt(dtResult.Rows[0]["dob_year"].ToString());

                    p.doj = dtResult.Rows[0]["doj"].ToString();

                    p.doj_date = dtResult.Rows[0]["doj_date"].ToString();
                    p.doj_month = dtResult.Rows[0]["doj_month"].ToString();
                    p.doj_year = convertToInt(dtResult.Rows[0]["doj_year"].ToString());

                    p.qualification = dtResult.Rows[0]["qualification"].ToString();


                }
                return p;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }


        public airtel_emp_tree get_airtel_emp_tree(string user_id)
        {
            try
            {

                DataAccess da = new DataAccess();
                airtel_emp_tree emp_tree = new airtel_emp_tree();
                DataTable dtResult = new DataTable();
                dtResult = da.ExecuteDataTable("get_airtel_emp_tree"
                                , da.Parameter("_user_id", user_id)
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    emp_tree.full_name = dtResult.Rows[0]["fullname"].ToString();
                    emp_tree.mobile_no = dtResult.Rows[0]["mobile_no"].ToString();
                    emp_tree.dist_drp_no = dtResult.Rows[0]["dist_drp_no"].ToString();
                    emp_tree.disti_name = dtResult.Rows[0]["disti_name"].ToString();
                    emp_tree.district = dtResult.Rows[0]["district"].ToString();
                    emp_tree.district_type = dtResult.Rows[0]["district_type"].ToString();
                    emp_tree.tm_name = dtResult.Rows[0]["tm_name"].ToString();
                    emp_tree.zone = dtResult.Rows[0]["zone"].ToString();
                    emp_tree.zsm_name = dtResult.Rows[0]["zsm_name"].ToString();
                }
                return emp_tree;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }

        public List<airtel_monthly_tracker> get_airtel_monthly_sales_tracker(string user_id)
        {
            try
            {

                DataAccess da = new DataAccess();

                List<airtel_monthly_tracker> tracker_list = new List<airtel_monthly_tracker>();
                DataTable dtResult = new DataTable();
                dtResult = da.ExecuteDataTable("get_airtel_monthly_tracker"
                                , da.Parameter("_user_id", user_id)
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        airtel_monthly_tracker tracker = new airtel_monthly_tracker();
                        tracker.company_id = convertToInt(dtResult.Rows[i]["company_id"].ToString());
                        tracker.displayname = dtResult.Rows[i]["displayname"].ToString();
                        tracker.cycle_id = convertToInt(dtResult.Rows[i]["cycle_id"].ToString());
                        tracker.cycle_name = dtResult.Rows[i]["cycle_name"].ToString();

                        tracker.lso_count_current_month = convertToInt(dtResult.Rows[i]["lso_count_current_month"].ToString());
                        tracker.sso_count_current_month = convertToInt(dtResult.Rows[i]["sso_count_current_month"].ToString());
                        tracker.total_fr_current_month = convertToInt(dtResult.Rows[i]["total_fr_current_month"].ToString());
                        tracker.high_value_fr_percent_ach_last_month_LM = convertToInt(dtResult.Rows[i]["high_value_fr_percent_ach_last_month_LM"].ToString());
                        tracker.high_value_fr_current_month = convertToInt(dtResult.Rows[i]["high_value_fr_current_month"].ToString());
                        tracker.high_value_fr_tgt_for_current_month = convertToInt(dtResult.Rows[i]["high_value_fr_tgt_for_current_month"].ToString());
                        tracker.high_value_fr_percent_ach_for_current_month = convertToInt(dtResult.Rows[i]["high_value_fr_percent_ach_for_current_month"].ToString());
                        tracker.high_value_fr_tgt_vs_ach = convertToInt(dtResult.Rows[i]["high_value_fr_tgt_vs_ach"].ToString());
                        tracker.high_value_fr_fse_on_tgt_vs_ach = convertToInt(dtResult.Rows[i]["high_value_fr_fse_on_tgt_vs_ach"].ToString());
                        tracker.high_value_fr_fse_on_LM_vs_mtd = convertToInt(dtResult.Rows[i]["high_value_fr_fse_on_LM_vs_mtd"].ToString());
                        tracker.quality_activations_percent_ach_LM = convertToInt(dtResult.Rows[i]["quality_activations_percent_ach_LM"].ToString());
                        tracker.quality_activations_lm = convertToInt(dtResult.Rows[i]["quality_activations_lm"].ToString());
                        tracker.total_activations_lm = convertToInt(dtResult.Rows[i]["total_activations_lm"].ToString());
                        tracker.quality_activation_tgt = convertToInt(dtResult.Rows[i]["quality_activation_tgt"].ToString());
                        tracker.quality_activation_percent_ach_lm = convertToInt(dtResult.Rows[i]["quality_activation_percent_ach_lm"].ToString());
                        tracker.qulity_activation_tgt_vs_ach = convertToInt(dtResult.Rows[i]["qulity_activation_tgt_vs_ach"].ToString());
                        tracker.quality_acitvation_fse_on_tgt_vs_ach = convertToInt(dtResult.Rows[i]["quality_acitvation_fse_on_tgt_vs_ach"].ToString());
                        tracker.quality_acitvation_fse_on_LM_vs_mtd = convertToInt(dtResult.Rows[i]["quality_acitvation_fse_on_LM_vs_mtd"].ToString());
                        tracker.mamo_tert_percent_ach_LM = convertToInt(dtResult.Rows[i]["mamo_tert_percent_ach_LM"].ToString());
                        tracker.mamo_tert_current_month = convertToInt(dtResult.Rows[i]["mamo_tert_current_month"].ToString());
                        tracker.mamo_tert_tgt = convertToInt(dtResult.Rows[i]["mamo_tert_tgt"].ToString());
                        tracker.mamo_tert_percent_ach_current_month = convertToInt(dtResult.Rows[i]["mamo_tert_percent_ach_current_month"].ToString());
                        tracker.percent_mamo_tert_tgt_vs_ach = convertToInt(dtResult.Rows[i]["percent_mamo_tert_tgt_vs_ach"].ToString());
                        tracker.fse_on_mamo_tert_tgt_vs_ach = convertToInt(dtResult.Rows[i]["fse_on_mamo_tert_tgt_vs_ach"].ToString());
                        tracker.fse_on_mamo_ter_lm_vs_mtd = convertToInt(dtResult.Rows[i]["fse_on_mamo_ter_lm_vs_mtd"].ToString());
                        tracker.total_score = convertToInt(dtResult.Rows[i]["total_score"].ToString());
                        tracker.qualified = dtResult.Rows[i]["qualified"].ToString();
                        tracker.incentive = convertToInt(dtResult.Rows[i]["incentive"].ToString());
                        tracker_list.Add(tracker);
                    }

                }
                return tracker_list;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }





    }



}
