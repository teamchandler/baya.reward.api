﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using ecomm.util.entities;
using System.Web;
using System.IO;
using System.Data;


namespace ecomm.api.Controllers
{
    public class hierarchyController : ApiController
    {
        [POST("/hierarchy/postImportParticipants/")]
        public string ImportParticipants(ImportParticipant oip)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                oc = hr.ImportParticipants(oip);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/hierarchy/postImportSIData/")]
        public string ImportSIData(SI_Data osd)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                oc = hr.ImportSIData(osd);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/hierarchy/postPopParticipants/")]
        public List<ImportParticipant> postPopParticipants(ImportParticipant oip)
        {
            ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
            return hr.PopParticipants(oip);
        }

        [POST("/hierarchy/postPopCompany/")]
        public List<COMPANY> postPopCompany()
        {
            ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
            return hr.PopCompany();

        }

        [POST("/hierarchy/postImportSalesDump/")]
        public string ImportSalesDump(distributor_sales_data odsd)
        {
            string oc = "";
            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                oc = hr.ImportSalesDump(odsd);
                if (oc.Length > 0)
                    return oc;
                else
                    return "No Response";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [POST("/hierarchy/postparticipantmaster/")]
        public DataTable postparticipantmaster()
        {

            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                return hr.get_participant_hierarchy(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [POST("/hierarchy/postparticipantrealtion/")]
        public DataTable postparticipantrealtion()
        {

            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                return hr.get_participant_hierarchy(2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [POST("/hierarchy/posthierarchy_config/")]
        public DataTable posthierarchy_config( hierarchy_conf ohc)
        {
            try
            {
                ecomm.model.repository.hierarchy_repository hr = new ecomm.model.repository.hierarchy_repository();
                return hr.get_hierarchy_config(ohc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
