﻿using AttributeRouting.Web.Http;
using ecomm.model.repository;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ecomm.api.Controllers
{
    public class checkoutController : ApiController
    {
        [POST("checkout/cart/")]
        public string postCartInfo(List<cart> crtlist)
        {
            checkoutrepository cr = new checkoutrepository();
            return cr.AddCartInfo(crtlist);
        }


        [POST("checkout/cartshipping/")]
        public string postCartShipping(shipping ship)
        {
            checkoutrepository cr = new checkoutrepository();
            return cr.AddShippingInfo(ship);
        }


        [POST("checkout/order/")]
        public string postShippingInfo(order ord)
        {
            checkoutrepository cr = new checkoutrepository();
            return cr.AddOrderBooking(ord);
        }

        [POST("payment/info/")]
        public string postShippingInfo(paymentinfostatus pis)
        {
            checkoutrepository cr = new checkoutrepository();
            return cr.ProcessOrderStatusAfterPG(pis);
        }

        [POST("user/redeempoint/")]
        public string user_redeem_point(user_point user_pont)
        {
            //ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();

            return "Redeem";
        }

    }
}
